ruleset trip_store{
  meta {
    provides trips, long_trips
    shares trips, long_trips

  }


  global{
    trips = function(){
      ent:trip
    }

    long_trips = function(){
      ent:long_trip
    }

  }

  rule collect_trips{
    select when explicit trip_processed mileage re#(.*)# t_s re#(.*)# setting(mil, t);
    always{
      ent:trip.append([mil, t])
    }
  }

  rule collect_long_trips{
    select when explicit found_long_trip mileage re#(.*)# t_s re#(.*)# setting(mil, t);
    always{
      ent:long_trip.append([mil, t])
    }
  }

  rule clear_trips{
    select when car reset;
    always{
      ent:trip := [];
      ent:long_trip := []
    }
  }
}
