ruleset track_trips{
  rule process_trip {
    select when echo message mileage re#(.*)# setting(mil);
    send_directive("trip") with
      trip_length = mil
      t_s = time:now()
  }
}
