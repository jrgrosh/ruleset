ruleset track_trips2{
  global {
    long_trip = 50
  }

  rule process_trip2 {
    select when car new_trip mileage re#(.*)# setting(mil);
    send_directive("trip") with
      trip_length = mil

    always{
      raise explicit event "trip_processed" attributes event:attrs()
    }
  }

  rule find_long_trips{
    select when explicit trip_processed mileage re#(.*)# setting(mil);
    pre {
      ats = event:attrs().klog("received trip_processed event, attributes: ")
    }
    if mil.as("Number").klog("Mileage of trip: ") > long_trip then
     noop()
    fired {
      raise explicit event "found_long_trip".klog("Explicit event found_long_trip raised")
    }
  }
}
